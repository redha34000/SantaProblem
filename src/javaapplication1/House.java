/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author chemali
 */
public class House {
    
   private int x ;     // ligne
    private int y ;    // colonne
    
     public House(){
        

    }
     
    public House(int x , int y){
        
        this.x = x ;
        this.y = y;
        
    }
    
    public void setX(int x){
       
        this.x = x;
        
    }
    
     public void setY(int y){
       
        this.y = y;
        
    }
    
     
     public int getX(){
         
         return this.x;
         
     }
     
      public int getY(){
      
        return this.y;
    }
      
          @Override
  public boolean equals(Object obj) {

        if (this == obj) {

            return true;

        } else if (obj == null) {

            return false;

        } else if (obj instanceof House) {

            House cust = (House) obj;

            if ( (cust.getX()==x  )&& cust.getY() == y) {

                return true;

            }

        }

        return false;

    }
  
  
  @Override
public int hashCode() {

    return this.x + this.y;
    
}
    
    
}
