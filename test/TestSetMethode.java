/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.HashSet;
import java.util.Set;
import javaapplication1.House;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chemali
 */
public class TestSetMethode {
    
    public TestSetMethode() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void TestSetMethode() {
    
   Set<House> test= new HashSet<>();      
    
   House t1 = new House();
   t1.setX(1);
   t1.setY(1);
   test.add(t1);
   
   t1.setX(1);
   t1.setY(2);
   test.add(t1);

   t1.setX(1);
   t1.setY(1);
   test.add(t1);
    
   assertEquals(test.size(), 2);        // s'assurer que notre set n'ajoute pas de doublon 
        
    
    
    }
    
}
