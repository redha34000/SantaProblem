# Résolution du probléme http://adventofcode.com/2015/day/3

# J'ai suivi 2 démarches que vous trouverez dans methode1 et methode2 avec une approche POO + test unitaire

# Pour les grids à 2 dimensions infinies dans la première démarche j'ai limité le nombre de cases car d'aprés ce que j'ai compris
infinie veut dire qu'on ne doit pas se soucier du problème si les cases sont dépassées

# Pour la limite de la boucle j'ai choisi un chiffre par défaut en supposant que la limite et la nuit de noel ( le nombre de maisons que
santa peut faire en une nuit )